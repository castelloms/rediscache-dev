# cache-dev

Example of Redis Cache

Postman examples

- GET http://localhost:8080/api/v1/cache/{key}
- POST http://localhost:8080/api/v1/cache
- DELETE http://localhost:8080/api/v1/cache/{key}
- POST http://localhost:8080/api/v1/cache/{timeout-seconds}