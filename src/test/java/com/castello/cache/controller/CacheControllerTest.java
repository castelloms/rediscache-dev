package com.castello.cache.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.castello.cache.RedisCacheApplication;
import com.castello.cache.model.CacheDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = RedisCacheApplication.class)
public class CacheControllerTest {

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	public void addCache() {
		CacheDTO cacheDTO = new CacheDTO();
		cacheDTO.setKey("sms");
		cacheDTO.setValue("1234");
		ResponseEntity<Void> post = testRestTemplate.postForEntity("/api/v1/cache", cacheDTO, Void.class);
		assertEquals(HttpStatus.OK, post.getStatusCode());
		ResponseEntity<String> getForEntity = testRestTemplate.getForEntity("/api/v1/cache/sms", String.class);
		assertEquals("1234", getForEntity.getBody());
	}

	@Test
	public void addCacheWithTimeout() throws InterruptedException {
		CacheDTO cacheDTO = new CacheDTO();
		cacheDTO.setKey("sms");
		cacheDTO.setValue("1234");
		Map<String, Integer> params = new HashMap<>();
		params.put("timeout-seconds", 3);
		ResponseEntity<Void> post = testRestTemplate.postForEntity("/api/v1/cache/{timeout-seconds}", cacheDTO,
				Void.class, params);
		assertEquals(HttpStatus.OK, post.getStatusCode());
	}

	@Test
	public void getCache() {
		ResponseEntity<String> get = testRestTemplate.getForEntity("/api/v1/cache/sms", String.class);
		assertEquals(HttpStatus.OK, get.getStatusCode());
	}

	@Test
	public void deleteCache() {
		Map<String, String> params = new HashMap<>();
		params.put("key", "sms");
		testRestTemplate.delete("/api/v1/cache/{key}", params);
	}

}
