package com.castello.cache.redis.service;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

@Service
public class CacheServiceImpl implements CacheService {

	@Autowired
	private StringRedisTemplate redisTemplate;

	@Override
	public void put(String key, String value) {
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		opsForValue.set(key, value);
	}

	@Override
	public void putWithTimeout(String key, String value, int timeout) {
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		opsForValue.set(key, value, timeout, TimeUnit.SECONDS);
	}

	@Override
	public String get(String key) {
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		return opsForValue.get(key);
	}

	@Override
	public void delete(String key) {
		redisTemplate.delete(key);
	}

}
