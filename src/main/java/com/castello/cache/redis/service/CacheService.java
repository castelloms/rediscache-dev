package com.castello.cache.redis.service;

public interface CacheService {

	String get(String key);

	void put(String key, String value);

	void putWithTimeout(String key, String value, int timeout);

	void delete(String key);


}
