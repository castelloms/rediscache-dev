package com.castello.cache.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.castello.cache.model.CacheDTO;
import com.castello.cache.redis.service.CacheService;

@Controller
@RequestMapping(value = "/api/v1")
public class CacheController {

	@Autowired
	private CacheService cacheService;

	@PostMapping(value = "/cache", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> addCache(@RequestBody(required = true) CacheDTO cacheDTO) {
		cacheService.put(cacheDTO.getKey(), cacheDTO.getValue());
		return ResponseEntity.ok().build();
	}

	@PostMapping(value = "/cache/{timeout-seconds}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> addCacheWithTimeout(@RequestBody(required = true) CacheDTO cacheDTo,
			@PathVariable(value = "timeout-seconds") int timeout) {
		cacheService.putWithTimeout(cacheDTo.getKey(), cacheDTo.getValue(), timeout);
		return ResponseEntity.ok().build();
	}

	@GetMapping(value = "/cache/{key}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> getCache(@PathVariable(value = "key") String key) {
		String value = cacheService.get(key);
		return ResponseEntity.ok(value);
	}

	@DeleteMapping(value = "/cache/{key}")
	public ResponseEntity<Void> deleteCache(@PathVariable(value = "key") String key) {
		cacheService.delete(key);
		return ResponseEntity.accepted().build();
	}

}
